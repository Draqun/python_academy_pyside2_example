""" Presentation layer module """

__all__ = ["App"]
__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__name__ = "gui"
__version__ = '1.1'

from PyQt5.QtCore import Qt
from PyQt5 import QtWidgets as QW
from pycalc.logic import Logic, LogicException
import sys


class AppGui(QW.QMainWindow):
    """ Main window class """
    buttons_labels = ['C', chr(8592), 'Undo', '', '7', '8', '9', '/', '4', '5', '6', '*', '1', '2', '3', '-', '0',
                      '.', '=', '+']

    def __init__(self):
        super(AppGui, self).__init__()

        self.__set_main_window_properties()
        self.__create_menu_bar()
        self.__set_central_widget()
        self.__create_field()
        self.__create_buttons()

        self.logic = Logic()

    def __set_main_window_properties(self):
        """ Set main window properties """
        self.setWindowTitle("QtCalculator")

    def __set_central_widget(self):
        """ Set central_widget """
        self.__central_widget = QW.QWidget(self)
        self.__central_widget_layout = QW.QVBoxLayout(self.__central_widget)

        self.__central_widget.setLayout(self.__central_widget_layout)
        self.setCentralWidget(self.__central_widget)

    def __create_menu_bar(self):
        """ Create application menu bar """
        self.__menu_bar = self.menuBar()
        menu = self.__menu_bar.addMenu("QtCalculator")
        exit_action = QW.QAction('Exit', parent=self)
        exit_action.triggered.connect(self.close_application)
        menu.addAction(exit_action)

    def __create_field(self):
        """ Method create input field """
        self.__field = QW.QLineEdit(parent=self.__central_widget)
        self.__field.setAlignment(Qt.AlignRight)
        self.__central_widget_layout.addWidget(self.__field)

    def __create_buttons(self):
        """ Method create input buttons """

        buttons_positions = [
            (0, 0), (0, 1), (0, 2), (0, 3),
            (1, 0), (1, 1), (1, 2), (1, 3),
            (2, 0), (2, 1), (2, 2), (2, 3),
            (3, 0), (3, 1), (3, 2), (3, 3),
            (4, 0), (4, 1), (4, 2), (4, 3)
        ]

        self.__buttons_layout = QW.QGridLayout(self.__central_widget)

        for pos, button_label in enumerate(self.buttons_labels):
            if pos == 3:
                continue

            button = QW.QPushButton(button_label, parent=self.__central_widget)
            button.clicked.connect(self.__button_action(button_label))
            self.__buttons_layout.addWidget(button, buttons_positions[pos][0], buttons_positions[pos][1])

        self.__central_widget_layout.addLayout(self.__buttons_layout)

    def __button_action(self, button_label):
        """ Button action handling method """
        def button_action():
            """ Real button action method """
            self.__on_button_clicked(button_label)

        return button_action

    def __on_button_clicked(self, button):
        """ On clicked button """
        if button == "=":
            try:
                result = self.logic.calculate(self.__field.text())
                print(result)
            except LogicException as le:
                print(le)
                QW.QMessageBox.critical(self, "Incorrect expression", "Error: {}".format(str(le)),
                                        QW.QMessageBox.Cancel, QW.QMessageBox.Cancel)
            else:
                self.__field.setText(str(result))
        elif button == "C":
            self.__field.setText("")
        elif button == chr(8592):
            self.__field.setText(self.__field.text()[:-1])
        elif button == "Undo":
            try:
                restored_text = self.logic.get_last_expression()
            except LogicException:
                pass
            else:
                self.__field.setText(restored_text)
        else:
            text = self.__field.text()
            text += button
            self.__field.setText(text)

    @staticmethod
    def close_application():
        """ Exit menu entry action """
        sys.exit(0)

    def show(self) -> None:
        """ Show main window """
        super(AppGui, self).show()
        self.setLayout(self.__central_widget_layout)


class App:
    """ QtCalculator class App """
    def __init__(self, *args, **kwargs):
        self.app = QW.QApplication(*args, **kwargs)

    def run(self):
        """ Run window application """
        app_gui = AppGui()
        app_gui.show()
        return self.app.exec_()
