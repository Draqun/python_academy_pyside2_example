#!/usr/bin/env python3

__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__version__ = '1.1'

from tests.pycalc_tests.logic_tests import LogicTest


if "__main__" == __name__:
    unittest.main()
