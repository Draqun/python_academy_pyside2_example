#!/usr/bin/env bash
coverage run -m unittest discover -s tests/
coverage html -d cov
