#!/usr/bin/env python3

__author__ = 'Damian Giebas'
__copyright___ = "Copyright (c) 2019 Damian Giebas"
__email__ = 'damian.giebas@gmail.com'
__version__ = '1.1'

from pycalc.app import App
import sys


if "__main__" == __name__:
    pycalc = App(sys.argv)
    sys.exit(pycalc.run())
